<?php /* $Id$
         CristalX4Drupal Theme
      */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" lang="<?php print $language ?>">

<head>

<base href="<?php print $GLOBALS['base_url']; ?>/" />

<title><?php print $head_title ?></title>

<?php print $head ?>

<?php print $styles ?>

<?php print $scripts ?>

<style type="text/css" media="print">@import "<?php print base_path() . path_to_theme() ?>/print.css";</style>

<!--[if lt IE 7]>
<style type="text/css" media="all">@import "<?php print base_path() . path_to_theme() ?>/fix-ie.css";</style>
<![endif]-->

</head>

<body class="sidebar-right">

<div id="jump-content">
<a href="#center" title="<?php print t('skip directly to the content') ?>" accesskey="2"><?php print t('skip to content') ?></a>
</div>

<div id="wrapper">
<div id="container" class="clear-block">

<div id="header">

<div id="logo-floater">
<?php
  // Prepare header
  if ($site_name)
    $site_name_title = check_plain($site_name);
  if ($site_slogan)
    $site_slogan_title = check_plain($site_slogan);

  $site_title = $site_name_title ? $site_name_title : ($site_slogan_title ? $site_slogan_title : null);

  if ($logo)
    print '<a href="'. check_url($base_path) .'" title="'. $site_title .'" class="site-logo"><img src="'. check_url($logo) .'" alt="'. $site_title .'" /></a>';

  if ($site_title) {
    print '<a href="'. check_url($base_path) .'" title="'. $site_title .'" class="site-title"><h1>';
    if ($site_name_title)
      print '<span class="site-name">'. $site_name_title .'</span>'. ($site_slogan_title ? ' ' : '');
    if ($site_slogan_title)
      print '<span class="site-slogan">'. $site_slogan_title .'</span>';
    print '</h1></a>';
  }
?>

</div>

<?php
  if (!isset($primary_links)) {
    $primary_links = array();
    $primary_links['none'] = array('title' => '');
  }
  print theme('links', $primary_links, array('class' => 'links primary-links'));
?>


<?php if ($search_box): ?>
<div id="search-floater" class="noprint">
<?php print $search_box ?>
</div>
<?php endif; ?>

</div>

<div id="center"><div id="squeeze"><div class="right-corner"><div class="left-corner"><div class="inner">

<?php if ($breadcrumb): print $breadcrumb; endif; ?>
<?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>

<div id="header-region">
<?php print $header; ?>

</div>

<?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>

<?php if ($title): print '<h2 class="main-title'. ($tabs ? ' with-tabs' : '') .'">'. $title .'</h2>'; endif; ?>

<?php if ($tabs): print $tabs; endif; ?>

<?php if (isset($tabs2)): print $tabs2; endif; ?>


<div class="inner-content">
<?php if ($help): print $help; endif; ?>
<?php if ($messages): print $messages; endif; ?>
<?php print $content ?>

</div>
<?php if ($tabs): print '</div>'; endif; ?>

<span class="clear"></span>

<?php print $feed_icons ?>

</div></div></div></div></div>

<div id="sidebar-right" class="sidebar">
<?php if ($sidebar_right): print $sidebar_right; endif; ?>
<?php if ($sidebar_left): print $sidebar_left; endif; ?>
</div>

<div id="footer">
<?php print $footer_message ?>
</div>

</div>
</div>

<div id="powered" class="noprint">
<?php print t('Powered by !Drupal. !CristalX theme created by !author.', array('!Drupal' => l('Drupal', 'http://drupal.org', array('title' => 'Drupal :: Community Plumbing')), '!CristalX' => l('CrystalX', 'http://www.oswd.org/design/preview/id/3465', array('title' => 'Open Source Web Design :: CrystalX')), '!author' => l('Nuvio | Webdesign', 'http://www.nuvio.cz', array('title' => 'vit.dlouhy')))); ?>
</div>

<?php print $closure ?>

</body>

</html>

