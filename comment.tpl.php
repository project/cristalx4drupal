<?php /* $Id$
         CristalX4Drupal Theme
      */
?>
<div class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ($comment->status == COMMENT_NOT_PUBLISHED) ? ' comment-unpublished' : ''; print ' '. $zebra; ?>">

<?php print $picture ?>

<?php if ($comment->new): ?>
<a id="new"></a>
<span class="new"><?php print drupal_ucfirst($new) ?></span>
<?php endif; ?>

<h3 class="comment-title"><?php print $title ?></h3>

<?php if ($submitted): ?>
<span class="submitted">
<?php print t('!date — !username', array('!date' => format_date($comment->timestamp), '!username' => theme('username', $comment))); ?>
</span>
<?php endif; ?>

<div class="content">
<?php print $content ?>

</div>

<?php if ($links): ?>
<div class="links noprint"><?php print $links ?></div>
<?php endif; ?>

</div>


